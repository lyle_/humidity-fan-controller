/**
   Humidity-based fan control

   Runs on an ESP8266 development board and uses an LM22 humidity
   sensor to switch a relay controlling an AC outlet.
*/
// NOTE: You'll need to create this file and add your own credentials. See config.h.example.
#include "config.h"

// Adafruit Unified Sensor library
#include <Adafruit_Sensor.h>
// DHT sensor library by Adafruit
#include <DHT_U.h>
#include <DHT.h>
// Libraries provided via the ESP8266 board manager
#include <ESP8266WiFi.h>
// Includes for PubSubClient
#include <PubSubClient.h>
// Get current time from NTP
#include <time.h>
// Parse incoming JSON messages
#include <ArduinoJson.h>

// GMT -6
const long gmtOffset = -6 * 60 * 60;
// Observe DST is 3600, set to 0 otherwise
const long dstOffset = 3600;

// Temperature/humidity sensor configuration
#define DHTTYPE DHT22   // DHT 22  (AM2302), AM2321; other types are available
const int DHT_PIN = 14; // D5 of NodeMCU is GPIO14
DHT dht(DHT_PIN, DHTTYPE);
float humidityThreshold = 63.0;

// Analog output pin used to control the relay. D2 on NodeMCU is GPIO4
const int RELAY_PIN = 4;

// PubSubClient configuration
WiFiClient espClient;
PubSubClient pubSubClient(espClient);
const char* mqtt_server = MQTT_BROKER_URL;

// Sample every 15 minutes
// NOTE: the DHT sensor is slow, so readings shouldn't be less than 2s apart
unsigned long sampleInterval = 60000 * 15;
unsigned long lastSampleMillis = 0;

char jsonBuf[MQTT_MAX_PACKET_SIZE];
// LED in NodeMCU at pin GPIO16 (D0). LED is active LOW on ESP8266.
#define LED D0

StaticJsonDocument<200> jsonDoc;


// Connect to the MQTT broker
void mqtt_reconnect() {
  // Loop until we're reconnected
  while (!pubSubClient.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (pubSubClient.connect(MQTT_CLIENTID, MQTT_USERNAME, MQTT_PASSWORD)) {
      Serial.println("connected");
      bool subscribed = pubSubClient.subscribe(MQTT_CONFIG_TOPIC_ID);
      if (subscribed) {
        Serial.print("Re-subscribed to");
        Serial.println(MQTT_CONFIG_TOPIC_ID);
      } else {
        Serial.print("Error re-subscribing to ");
        Serial.println(MQTT_CONFIG_TOPIC_ID);
        pubSubClient.publish(MQTT_ERROR_TOPIC_ID, "error subscribing to config topic");
      }
    } else {
      Serial.print("failed, rc=");
      Serial.print(pubSubClient.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

// Diagnostic routine; the relay and the LED will turn on for two seconds to verify functionality.
void startupDiagnostic() {
  Serial.println("Startup diagnostic: enabling relay and onboard LED for two seconds");
  digitalWrite(LED, LOW);
  digitalWrite(RELAY_PIN, HIGH);
  delay(2000);
  digitalWrite(RELAY_PIN, LOW);
  Serial.println("Startup diagnostic complete");
}

// Set up on first load
void setup() {
  Serial.begin(115200);

  Serial.println("Configuring onboard LED");
  pinMode(LED, OUTPUT);

  Serial.println("Configuring relay output pin");
  pinMode(RELAY_PIN, OUTPUT);

  Serial.println("Initializing DHT sensor");
  dht.begin();

  Serial.print("Connecting to WiFi SSID:");
  Serial.println(WIFI_SSID);
  /* Explicitly set the ESP8266 to be a WiFi-client, otherwise, it by default,
     would try to act as both a client and an access-point and could cause
     network-issues with your other WiFi-devices on your WiFi-network. */
  WiFi.mode(WIFI_STA);
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.print("WiFi connected, IP address: ");
  Serial.println(WiFi.localIP());

  configTime(gmtOffset, dstOffset, "ntp1.doit.wisc.edu", "ntp1.doit.wisc.edu", "pool.ntp.org");
  Serial.println("Waiting for time");
  while (!time(nullptr)) {
    Serial.print(".");
    delay(1000);
  }
  Serial.println("");

  Serial.print("Setting up MQTT client for ");
  Serial.println(mqtt_server);
  pubSubClient.setServer(mqtt_server, 1883);
  pubSubClient.setCallback(messageReceived);

  startupDiagnostic();

  Serial.println("Setup complete");
}

void messageReceived(char* topic, byte* payload, unsigned int length) {
  if (length > MQTT_MAX_PACKET_SIZE) {
    pubSubClient.publish(MQTT_ERROR_TOPIC_ID, "received payload longer than MQTT_MAX_PACKET_SIZE");
  } else {
    Serial.print(F("=====> Received message to topic "));
    Serial.print(topic);
    Serial.println(F(": "));
    payload[length] = '\0';
    //    String s = String((char*)payload);
    // Deserialize the JSON document
    DeserializationError error = deserializeJson(jsonDoc, payload);
    // Test if parsing succeeds.
    if (error) {
      Serial.print(F("deserializeJson() failed: "));
      Serial.println(error.c_str());
      pubSubClient.publish(MQTT_ERROR_TOPIC_ID, "error parsing JSON payload");
      return;
    }
    if (jsonDoc.containsKey("humidityThreshold")) {
      Serial.print(F("Setting humidity threshold from "));
      Serial.print(humidityThreshold);
      Serial.print(F(" to "));
      Serial.println(jsonDoc["humidityThreshold"].as<float>());
      humidityThreshold = jsonDoc["humidityThreshold"];
    }
    if (jsonDoc.containsKey("sampleInterval")) {
      Serial.print(F("Setting sample interval from "));
      Serial.print(sampleInterval);
      Serial.print(F(" to "));
      Serial.println(jsonDoc["sampleInterval"].as<long>());
      sampleInterval = jsonDoc["sampleInterval"];
    }
  }
}

// Main loop
void loop() {
  unsigned long loopStartTime = millis();

  if (!pubSubClient.connected()) {
    mqtt_reconnect();
  }
  pubSubClient.loop();

  if (loopStartTime - lastSampleMillis >= sampleInterval) {
    Serial.print(loopStartTime - lastSampleMillis);
    Serial.println("ms elapsed since last sample taken, reading values");
    bool relayOn = false;
    // Reading temperature or humidity takes about 250 milliseconds;
    // sensor readings may also be up to 2 seconds 'old' (it's a very slow sensor)
    float h = dht.readHumidity();
    // Read temperature as Fahrenheit (isFahrenheit = true)
    float t = dht.readTemperature(true);

    // Check if any reads failed and exit early (to try again).
    if (isnan(h) || isnan(t)) {
      Serial.print("Failed to read DHT sensor");
      pubSubClient.publish(MQTT_ERROR_TOPIC_ID, "failed to read DHT sensor");
      return;
    }

    // Compute heat index
    float hif = dht.computeHeatIndex(t, h);

    // Switch relay based on humidity
    if (h >= humidityThreshold) {
      digitalWrite(RELAY_PIN, HIGH);
      digitalWrite(LED, LOW);
      relayOn = true;
    } else {
      digitalWrite(RELAY_PIN, LOW);
      digitalWrite(LED, HIGH);
    }

    // Emit current status
    const char* json = buildJSON(h, t, relayOn);
    Serial.println(json);
    bool result = pubSubClient.publish(MQTT_TOPIC_ID, json);
    if (!result) {
      pubSubClient.publish(MQTT_ERROR_TOPIC_ID, "failed to publish status");
    }
    lastSampleMillis = millis();
  }
}

// Construct JSON payload for MQTT message
const char* buildJSON(float h, float t, bool relayOn) {
  char hbuf[3];
  char hThreshBuf[3];
  char tbuf[3];
  dtostrf(h, 0, 0, hbuf);
  dtostrf(humidityThreshold, 0, 0, hThreshBuf);
  dtostrf(t, 0, 0, tbuf);

  // Get ISO8601 time
  time_t now = time(nullptr);
  char timebuf[sizeof "2011-10-08T07:07:09-06"];
  strftime(timebuf, sizeof timebuf, "%FT%T-06", gmtime(&now));

  // Compose JSON payload
  snprintf(jsonBuf, 256,
           "{"
           "\"humidity\":%s,"
           "\"humidityThreshold\":%s,"
           "\"temp\":%s,"
           "\"relayOn\":%s,"
           "\"timestamp\":\"%s\""
           "}",
           hbuf, hThreshBuf, tbuf, relayOn ? "true" : "false", timebuf);
  return jsonBuf;
}
