# Humidity-Based Fan Control

Controls a relay to turn on a fan when a certain level of humidity is detected.

Current functionality: the project will connect to a WiFi network and then read
temperature and humidity data from the DHT22 every 2 seconds, printing the data
to the serial console. If the humidity reading is above a preconfigured threshold
set in the code, it will activate an attached 120V relay, turning on the fan that
is plugged into the AC outlet being switched. It will also light up the onboard
LED on the ESP8266.

On startup, the switched relay and the onboard LED will both be enabled for two
seconds; this provides simple confirmation that the device is working properly.
They will stay on if the humidity threshold is met, or turn off otherwise.

## Description

This project runs on an ESP8266 board, particularly the [NodeMCU 1.0](https://nodemcu.readthedocs.io/en/master/)
(ESP-12E) version, but may run on others. The software is developed and flashed
using the [Arduino](https://www.arduino.cc/en/main/software) IDE.

See this useful [introduction video](https://www.makeuseof.com/tag/meet-arduino-killer-esp8266/)
for details on the platform.

The humidity sensor is a [DHT22](https://learn.adafruit.com/dht) type, although a
DHT11 can work as well.

I'm using a 5V relay board to switch the 120V AC power source [Amazon](https://www.amazon.com/gp/product/B07M88JRFY),
[datasheet](http://www.circuitbasics.com/wp-content/uploads/2015/11/SRD-05VDC-SL-C-Datasheet.pdf).

## Initial software setup

You'll first need to add the ESP8266 board manager to the Arduino IDE to enable
it to compile and flash code to the board. Follow these instructions:
https://www.instructables.com/id/Steps-to-Setup-Arduino-IDE-for-NODEMCU-ESP8266-WiF/

Once that is configured, you'll need to be sure the necessary sensor libraries
are installed in the IDE (these can be installed via `Tools->Manage Libraries`
within the IDE):

* Adafruit Unified Sensor (https://github.com/adafruit/Adafruit_Sensor)
* DHT sensor library by Adafruit (https://github.com/adafruit/DHT-sensor-library)
* PubSubClient by Nick O'Leary (https://github.com/knolleary/pubsubclient)
  *Note*: you'll need to modify the pubsubclient library to redefine `MQTT_MAX_PACKET_SIZE`
  to 256 (https://github.com/knolleary/pubsubclient/issues/110)
* ArduinoJson (https://arduinojson.org/)


Now you're all set to wire up the board and upload the code it to the device!

## Hardware wiring

The ESP8266 board is powered and programmed via the micro USB connector.

### DHT22 humidity sensor

Wire power and ground to a corresponding 3.3V power pin and a `GND` pin.
The signal wire should be connected to the D5 pin as currently configured in the code.

### 5V-120V relay

The low-voltage (5V) side should be wired to ground and the `Vin` pins on the ESP8266 for 5V.
The signal wire goes to the D5 pin on the ESP8266, which maps to GPIO pin 14 in
Arduino-speak.

On the switched 120V side of the relay, the hot wire from the power cord should be
connected to the Common terminal, and the NO (normally open) pin of the relay should
route back to the hot terminal on the switched outlet.  This means that the circuit
will be completed when the input pin of the relay recieves 5V, or a
[HIGH](https://www.arduino.cc/reference/en/language/variables/constants/constants/#_high)
signal from the Arduino code.

The ground and neutral from the power cord should be connected to the switched outlet
as usual.

Here's what it looks like wired to an Arduino:
![wiring the relay to the outle](relay_wiring.png)

See this [example project](http://www.circuitbasics.com/build-an-arduino-controlled-power-outlet/)
for more details.

## Configuration

Copy the `config.h.example` file to `config.h` (ignored by git) and replace the
credentials with your own. For an easy-to-use MQTT broker, I'd recommend signing
up for an account at [shiftr.io](shiftr.io).

## Publishing data via MQTT

Sensor data is published to an [MQTT](https://en.wikipedia.org/wiki/MQTT) broker
as configured in `config.h`.

## Consuming data via AWS Lambda

The `lambda.py` program included will poll the URL configured in Lambda and store
the data in a configured S3 bucket. That configuration is currently left as an
exercise for the reader.

## Changing configuration at runtime

In addition to publishing data, the device will subscribe to events which allow
it to change its configuration at runtime. For example (actual credentials
are required for this to work):

	curl -X POST 'http://try:try@broker.shiftr.io/homie/basement-fan/config?retained=true' -d '{"humidityThreshold":63,"sampleInterval":900000}'

These two values, `humidityThreshold` and `sampleInterval`, are currently the
only values configurable at runtime.

*Note*: Publishing the configuration message as [retained](https://www.hivemq.com/blog/mqtt-essentials-part-8-retained-messages/).
means that when the client (re)subscribes, it ~should~ automatically receive the latest
retained configuration message, if any. This is a handy way to override the
default configuration compiled into the device with a reasonable hope that
it'll reconfigure itself when it starts up.

## Viewing the data

The included `index.html` uses [Chart.js](https://www.chartjs.org/) to pull the
JSON data from the S3 bucket and charts it:

https://basement-fan.s3.us-east-2.amazonaws.com/index.html

We display historical sensor data as well as weather data from OpenWeatherMap
fetched by the above mentioned Lambda function.

## Using Arduino CLI

The Arduino IDE is okay when you're starting out, but using [arduino-cli](https://github.com/arduino/arduino-cli)
is a much better fit for my workflow and allows me to use (Neo)Vim to edit the code.
Some basic setup:

	# Install libraries
	arduino-cli lib install "Adafruit Unified Sensor"
	arduino-cli lib install "ArduinoJson"
	arduino-cli lib install "DHT sensor library"
	arduino-cli lib install "PubSubClient"
	# Compile; this assumes you've installed the proper core for your board according to the arduino-cli instructions.
	# For an Arduino Uno, for example, the fqbn would be "arduino:avr:uno".
	arduino-cli compile --fqbn esp8266:esp8266:nodemcu .
	# Flash the board
	arduino-cli upload -p /dev/ttyUSB0 --fqbn esp8266:esp8266:nodemcu .

## Gotchas

* When wiring up the pins and assigning them in code, the GPIO pin numbers mapped
in software don't match the pin numbers printed on the board. See the
[NodeMCU GPIO reference](https://nodemcu.readthedocs.io/en/master/modules/gpio/)
for mappings.

* Compiling and loading the sketch from the Arduino IDE will fail under JDK8.
  If you get a Java error trying to flash the sketch, switch to a newer JDK
  (JDK11 is known to work) and restart the Arduino IDE.

* Trying to call `delay()` during the main loop causes disconnection issues and
  incoming messages may not be received. We take samples at a fixed, arbitrary
  interval, but keep the main loop moving so that the MQTT connection is maintained.

## ESP8266 References:

* [Build a $5 IoT Thing with ESP8266 and AWS IoT Core](https://www.youtube.com/watch?v=B82Arj_66Pc) (YouTube)
