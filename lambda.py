import json
import boto3
import os
from botocore.vendored import requests

BUCKET_BASE     = 'basement-fan'
TEMP_FILE       = '/tmp/temperatures.json'
MAX_READINGS    = int((60)*365*5) # one reading every minute for 5 years
MAX_READINGS    = MAX_READINGS - 1 # make it easy for calculations

# When we add new fields to an existing dataset, we want to pre-populate data points to match the existing number of timestamps.
def normalize_fields(data, field_name):
    ts_count = len(data['timestamp'])
    new_field_count = 0
    try:
        new_field_count = len(data[field_name])
    except:
        pass
    if new_field_count < ts_count:
        print("Timestamp count: " + str(ts_count) + ", " + field_name + " count: " + str(new_field_count) + ", prepopulating the difference")
        difference = ts_count - new_field_count
        data[field_name] = [-1] * difference

def get_existing_values(filename):
    s3 = boto3.resource('s3')
    existing_values = {"humidity": [], "humidityThreshold": [], "temp": [], "relayOn": [], "rain_mm_1h": [], "outside_temp": [], "outside_humidity": [], "timestamp": []}
    try:
        s3.Bucket(BUCKET_BASE).download_file(filename, TEMP_FILE)
        f = open(TEMP_FILE)
        existing_values = json.loads(f.read())
        f.close()
        normalize_fields(existing_values, "rain_mm_1h")
        normalize_fields(existing_values, "outside_temp")
        normalize_fields(existing_values, "outside_humidity")
    except Exception as e:
        if e.response['Error']['Code'] == '404':
            print('No previous record for this device.')
        else:
            raise(e)
    return existing_values


def append_current_weather(existing_values):
    openweathermap_token = os.environ['OPENWEATHERMAP_TOKEN']
    url = "http://api.openweathermap.org/data/2.5/weather?zip=53704&units=imperial&appid=" + openweathermap_token
    weather_json = json.loads(requests.get(url).text)
    rain_mm_1h = 0
    try:
        rain_mm_1h = weather_json['main']['rain']['1h']
    except:
        pass
    outside_temp = None
    try:
        outside_temp = weather_json['main']['temp']
    except:
        print("Error reading current outdoor temp")
    outside_humidity = None
    try:
        outside_humidity = weather_json['main']['humidity']
    except:
        print("Error reading current outdoor humidity")

    existing_values["rain_mm_1h"].append(rain_mm_1h)
    existing_values["outside_temp"].append(outside_temp)
    existing_values["outside_humidity"].append(outside_humidity)
    print("Rain: " + str(existing_values['rain_mm_1h'][-1]))
    print("Outside Temp: " + str(existing_values['outside_temp'][-1]))
    print("Outside Humidity: " + str(existing_values['outside_humidity'][-1]))

def append_values_to_s3_object(filename, existing_values, values_to_append):
    # Let's not store more than MAX_READINGS readings. Trim oldest if necessary.
    h_len = len(existing_values["humidity"])
    if h_len > MAX_READINGS:
        existing_values["humidity"] = existing_values["humidity"][h_len-MAX_READINGS:]

    ht_len = len(existing_values["humidityThreshold"])
    if ht_len > MAX_READINGS:
        existing_values["humidityThreshold"] = existing_values["humidityThreshold"][ht_len-MAX_READINGS:]

    t_len = len(existing_values["temp"])
    if t_len > MAX_READINGS:
        existing_values["temp"] = existing_values["temp"][t_len-MAX_READINGS:]

    r_len = len(existing_values["relayOn"])
    if r_len > MAX_READINGS:
        existing_values["relayOn"] = existing_values["relayOn"][r_len-MAX_READINGS:]

    ts_len = len(existing_values["timestamp"])
    if ts_len > MAX_READINGS:
        existing_values["timestamp"] = existing_values["timestamp"][ts_len-MAX_READINGS:]

    existing_values["humidity"].append(values_to_append["humidity"])
    existing_values["humidityThreshold"].append(values_to_append["humidityThreshold"])
    existing_values["temp"].append(values_to_append["temp"])
    existing_values["relayOn"].append(values_to_append["relayOn"])
    existing_values["timestamp"].append(values_to_append["timestamp"])

    append_current_weather(existing_values)

    s3 = boto3.resource('s3')
    s3.Bucket(BUCKET_BASE).put_object(Body = json.dumps(existing_values), Key = filename, ACL='public-read')

def lambda_handler(event, context):
    filename = 'basement-humidity-fan.json'
    existing_values = get_existing_values(filename)
    if 'humidity' in event.keys():
        print("Got humidity data in payload, appending", event)
        append_values_to_s3_object(filename, existing_values, event)
    else:
        url = os.environ['MQTT_URL']
        print("No humidity data in payload, fetching from " + url)
        basicAuthCredentials = (os.environ['MQTT_USERNAME'], os.environ['MQTT_PASSWORD'])
        event = json.loads(requests.get(url, auth=basicAuthCredentials).text)
        print("Appending " + str(event))
        append_values_to_s3_object(filename, existing_values, event)
